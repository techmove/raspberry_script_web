#!/bin/bash
#=====================================================================+
#		SCRIPT DE INSTALACAO AGENTE ZABBIX		
#=====================================================================+
#CRIADO POR	: LUIZ PAULO PIMENTA
#DATA		: 30/03/2022 
#=====================================================================+

clear

source ./includes.sh


echo -e $WHITE =============================================================== $FECHA
echo -e $GREEN   INICIANDO A INSTALAÇÃO DAS CONFIGURACOES DO RASPBERRY CLUBE CURITIBANO$$ $FECHA
echo -e $WHITE =============================================================== $FECHA
echo -e $GREEN SCRIPT CRIADO POR LUIZ PAULO PIMENTA $FECHA


echo .
echo .

echo -e $GREEN AJUSTE REPOSITORIO RASPK $FECHA
cp arquivos/sources.list /etc/apt/

echo .
echo .

echo -e $GREEN EFETUA UPDATE DE DO SISTEMA E INSTALA PACOTES UTEIS $FECHA
apt update
apt install aptitude git vim -y

echo .
echo .

echo -e $GREEN CONFIRMA INSTALACAO NAVEGADOR CHROMIUM $FECHA
apt install chromium-sandbox -y

echo .
echo .

echo -e $GREEN ADICIONANDO SCRIPT PARA INICIALIZAR AO LOGIN DA MAQUINA $FECHA
echo /usr/local/src/raspberry_script_web/scripts/web.sh >> /home/pi/.profile
